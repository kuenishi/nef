#include "nef.h"

#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

#include <unistd.h>

#include <sys/socket.h>

#include "erl_driver.h"

#include <errno.h>
#include <fcntl.h>

#include <netinet/in.h>

#include <string.h> // memset

#define TUPLE2_A_I(e, atom, i) \
  enif_make_tuple2((e), enif_make_atom((e),(atom)),enif_make_int((e), (i)))

#define TUPLE2_E(e, errnumber) \
  enif_make_tuple2((e), enif_make_atom((e),"error"),erl_errno_id(errnumber))

int on_load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM info) {
  return 0;
}


int on_reload(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM info) {
  return 0;
}

int on_upgrade(ErlNifEnv* env, void** priv_data, void** old_data, ERL_NIF_TERM info) {
  return 0;
}

ERL_NIF_TERM nef_ping(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]){
  return enif_make_atom(env, "ok");
}

ERL_NIF_TERM nef_poll_create(ErlNifEnv* env, int arg, const ERL_NIF_TERM argv[]){
  int fd = kqueue();
    //epoll_create(64);
  if(fd < 0){
    return TUPLE2_E(env, errno);
  }else{
    return TUPLE2_A_I(env,"ok", fd);
  }
}

ERL_NIF_TERM nef_poll_wait(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  return enif_make_atom(env, "error");
}
ERL_NIF_TERM nef_add(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  
  return enif_make_atom(env, "error");
}
ERL_NIF_TERM nef_del(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  return enif_make_atom(env, "error");
}

ERL_NIF_TERM nef_close(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  int fd = -1;
  enif_get_int(env, argv[0], &fd); //FIXME - error handling
  close(fd);
  return enif_make_atom(env,"ok");
}

ERL_NIF_TERM nef_socket(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  int domain = 0;
  int domain0 = 0;
  int type = 0;
  int type0 = 0;
  int s;
  enif_get_int(env, argv[0], &domain); //FIXME - error handling
  enif_get_int(env, argv[1], &type); //FIXME 
  if(domain | NEF_PF_INET){
    domain0 |= PF_INET;
  }else if(domain | NEF_PF_INET6){
    domain0 |= PF_INET6;
  }else if(domain | NEF_PF_UNIX){
    domain0 |= PF_UNIX;
  }else{
    return enif_make_badarg(env);
  }
  if(type | NEF_SOCK_STREAM ){
    type0 = SOCK_STREAM;
  }else if(type | NEF_SOCK_DGRAM){
    type0 = SOCK_DGRAM;
  }else{
    return enif_make_badarg(env);
  }
  if((s=socket(domain0, type0, 0)) > 0){
    int flags = fcntl(s, F_GETFL, 0); // make it nonblocking
    if(fcntl(s, F_SETFL, O_NONBLOCK|flags) != -1)
      return TUPLE2_A_I(env,"ok", s);
  }
  return TUPLE2_E(env, errno);
}


ERL_NIF_TERM nef_listen(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  int fd;
  int arity;
  const ERL_NIF_TERM *array;
  unsigned int port;
  unsigned int a,b,c,d;
  struct sockaddr_in addr;
  
  if(!enif_get_int(env, argv[0], &fd)){
    return enif_make_badarg(env);
  }
  if(!enif_get_tuple(env, argv[1], &arity, &array)){
    return enif_make_badarg(env);
  }
  if(arity != 4
     || !enif_get_uint(env, array[0], &a)
     || !enif_get_uint(env, array[1], &b)
     || !enif_get_uint(env, array[2], &c)
     || !enif_get_uint(env, array[3], &d)){ // IPv4 only
    return enif_make_badarg(env);
  }
  if(!enif_get_uint(env, argv[2], &port)){
    return enif_make_badarg(env);
  }
  
  memset(&addr, 0, sizeof(struct sockaddr_in));
  addr.sin_port = port;
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = INADDR_ANY; //FIXME
  
  //  printf("%d.%d.%d.%d:%d\n", a, b, c, d, port);

  // bind
  if( bind(fd, (struct sockaddr*)&addr, sizeof(struct sockaddr_in)) == -1 ){
    return TUPLE2_E(env, errno);
  }
  // listen
  if( listen(fd, 65535) == -1 ){
    return TUPLE2_E(env, errno);
  }
  return enif_make_atom(env, "ok");
}


ERL_NIF_TERM nef_accept(ErlNifEnv* env, int args, const ERL_NIF_TERM argv[]){
  int fd;
  struct sockaddr_in addr;
  socklen_t sockaddr_len;
  int fd2;
  
  if(!enif_get_int(env, argv[0], &fd)){
    return enif_make_badarg(env);
  }
  if((fd2=accept(fd, (struct sockaddr*)&addr, &sockaddr_len)) == -1){
    return TUPLE2_E(env, errno);
  }
  return TUPLE2_A_I(env, "ok", fd2);
}
