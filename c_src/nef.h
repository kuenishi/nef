#ifndef NEF_H
#define NEF_H

#include "erl_nif.h"

#if ERL_NIF_MAJOR_VERSION != 2
#error
#endif

int on_load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM info);

int on_reload(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM info);

int on_upgrade(ErlNifEnv* env, void** priv_data, void** old_data, ERL_NIF_TERM info);

ERL_NIF_TERM nef_ping(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]);


ERL_NIF_TERM nef_poll_create(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM nef_poll_wait(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM nef_add(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM nef_del(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM nef_close(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM nef_socket(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);

ERL_NIF_TERM nef_listen(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);
ERL_NIF_TERM nef_accept(ErlNifEnv*, int, const ERL_NIF_TERM argv[]);

static ErlNifFunc nif_functions[] = {

  {"poll_create", 0, nef_poll_create},
  //{"poll_wait", 1, nef_poll_wait},
  //{"poll_wait", 2, nef_poll_wait},
  //{"add", 2, nef_add},
  //{"del", 2, nef_del},
  {"close", 1, nef_close},
  
  {"socket_", 2, nef_socket},
  {"listen", 3, nef_listen},
  {"accept", 1, nef_accept},

  //{"read", 1, nef_read},
  //{"write", 2, nef_write},

  {"ping", 0, nef_ping}
};

ERL_NIF_INIT(nef, nif_functions, &on_load, &on_reload, &on_upgrade, NULL);

#define NEF_PF_INET   (1)
#define NEF_PF_INET6  (2)
#define NEF_PF_UNIX   (4)
#define NEF_SOCK_DGRAM   (8)
#define NEF_SOCK_STREAM  (0x10)

#endif

