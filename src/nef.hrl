
-define(PF_INET,  1).
-define(PF_INET6, 2).
-define(PF_UNIX,  4).
-define(SOCK_DGRAM,  16#8).
-define(SOCK_STREAM, 16#10).
