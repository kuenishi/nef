%%%-------------------------------------------------------------------
%%% @author UENISHI Kota <kuenishi@gmail.com>
%%% @copyright (C) 2011, UENISHI Kota
%%% @doc
%%%
%%% @end
%%% Created : 11 May 2011 by UENISHI Kota <kuenishi@gmail.com>
%%%-------------------------------------------------------------------
-module(nef).
-author('@kuenishi').

-include("nef.hrl").

-type fd() :: non_neg_integer().
-export([ping/0]).

% TCP socket
-export([socket/1, listen/3, accept/1]).
-export([connect/2, send/2, recv/2]).
% UDP socket
% -export([open/1, recv/1, send/2, close/1]).
% SCTP socket
% -export( ??? ).
% UDS
% Files
% -export([open/2, read/2, write/2, close/1]).

% Timer FD - nothing to do with it.

% poller
-export([poll_create/0, poll_wait/1, poll_wait/2,
	 add/2, del/2, close/1]).

-on_load(init/0).

init()->
    SoName = case code:priv_dir(?MODULE) of
		 {error, bad_name} ->
		     case filelib:is_dir(filename:join(["..", "priv"])) of
			 true ->
			     filename:join(["..", "priv", "nef"]);
			 false ->
			     filename:join(["priv", "nef"])
		     end;
		 Dir ->
		     filename:join(Dir, "nef")
	     end,
    R=erlang:load_nif(SoName, 0),
    R.


ping()->
    exit(nef_not_loaded).

-spec poll_create()-> {ok,fd()}|{error,term()}.
poll_create()->
    exit(nef_not_loaded).

-spec poll_wait(fd()) -> {ok, [term()]} | {error, term()}.
poll_wait(_)->
    exit(nef_not_loaded).

-spec poll_wait(fd(), timeout()) -> {ok, [term()]} | {error, term()}.
poll_wait(_,_)->
    exit(nef_not_loaded).

-spec add(fd(), [{fd(), term()}]) -> ok.
add(_, _)->
    exit(nef_not_loaded).

-spec del(fd(), [fd()]) -> ok.
del(_, _)->
    exit(nef_not_loaded).

-spec close(fd())-> ok.
close(_)->
    exit(nef_not_loaded).

% accept inet/inet6/unix  tcp/udp
-spec socket([term()])-> {ok,fd()}|{error,term()}.
socket(Opts)->
    socket(Opts, 0, 0).

socket([], 0, _) -> {error, badarg};
socket([], _, 0) -> {error, badarg};
socket([], T, P) -> socket_(T, P);
socket([inet|Opts], T, P) -> socket(Opts, T bor ?PF_INET, P);
socket([inet6|Opts],T, P) -> socket(Opts, T bor ?PF_INET6, P);
socket([unix|Opts], T, P) -> socket(Opts, T bor ?PF_UNIX, P);
socket([tcp|Opts], T, P) ->  socket(Opts, T, P bor ?SOCK_STREAM);
socket([udp|Opts], T, P) ->  socket(Opts, T, P bor ?SOCK_DGRAM);
socket([O|_], _, _) -> {error, {badarg,O}}.

socket_(_,_)->
    exit(nef_not_loaded).

-spec listen(fd(), inet:ip_address(), pos_integer())-> ok | {error, term()}.
listen(_,_,_)->
    exit(nef_not_loaded).

-spec accept(fd())-> ok | {error, term()}.
accept(_)->
    exit(nef_not_loaded).

connect(_,_)->
    exit(nef_not_loaded).

send(_,_)->
    exit(nef_not_loaded).

recv(_,_)->
    exit(nef_not_loaded).
