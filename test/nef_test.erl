%%%-------------------------------------------------------------------
%%% @author UENISHI Kota <kuenishi@gmail.com>
%%% @copyright (C) 2011, UENISHI Kota
%%% @doc
%%%
%%% @end
%%% Created : 11 May 2011 by UENISHI Kota <kuenishi@gmail.com>
%%%-------------------------------------------------------------------
-module(nef_test).

-include_lib("eunit/include/eunit.hrl").

easy_test()->
    ?assertEqual(ok,nef:ping()),

    {ok,Fd} = nef:poll_create(),
    ?assert(Fd > 2),

    ok=nef:close(Fd),

    {ok, Fd0} = nef:socket([inet,tcp]),
%    ?debugHere,
    ok=nef:listen(Fd0, {0,0,0,0}, 9187),
    % {ok,Fd1} = nef

    ?assert(Fd0 > 2),
    ok=nef:close(Fd0).
